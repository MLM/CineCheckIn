package com.example.mikael.cinemauclerc;

/**************************************************************************************************
 File name :Ticket.java
 Version : 0.2
 Purpose : Application Android pour vérifier la validité de tickets acheter en ligne.
 Usages : see README
 Date : 29/02/2016
 Author : mikael.lemasson@orange.fr
 Android : 4.0.2
 License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
 ***************************************************************************************************/

public class Ticket {

    private int id = 0;
    private String numtt = "";
    private String date = "";
    private String status = "";

    public Ticket(){}

    public Ticket(int id, String numtt, String date, String status){
        this.id = id;
        this.numtt = numtt;
        this.date = date;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumtt() {
        return numtt;
    }

    public void setNumtt(String numtt) {
        this.numtt = numtt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String toString(){
        return "ID : "+id+" Numtt: "+numtt+" Date: "+date+" Status: "+status;
    }
}