package com.example.mikael.cinemauclerc;

/**************************************************************************************************
 File name :DatabaseHandler.java
 Version : 0.2
 Purpose : Application Android pour vérifier la validité de tickets acheter en ligne.
 Usages : see README
 Date : 29/02/2016
 Author : mikael.lemasson@orange.fr
 Android : 4.0.2
 License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
 ***************************************************************************************************/

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.SQLException;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

    static final String TAG = "DatabaseHandler";
    private static final String TABLE_TICKETS = "table_tickets";
    private static final String COL_ID = "ID";
    private static final String COL_NUMTT = "NUMTT";
    private static final String COL_DATE = "DATE";
    private static final String COL_STATUS = "STATUS";

    private static final String CREATE_BDD = "CREATE TABLE " + TABLE_TICKETS + " ("
            + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NUMTT + " TEXT NOT NULL, "
            + COL_DATE + " TEXT NOT NULL, " + COL_STATUS + " TEXT NOT NULL);";

    public DatabaseHandler(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //on créé la table à partir de la requête écrite dans la variable CREATE_BDD
        db.execSQL(CREATE_BDD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //On peut fait ce qu'on veut ici moi j'ai décidé de supprimer la table et de la recréer
        //comme ça lorsque je change la version les id repartent de 0
 /*       db.execSQL("DROP TABLE IF EXISTS " + TABLE_TICKETS + ";");
        onCreate(db);*/
    }

}