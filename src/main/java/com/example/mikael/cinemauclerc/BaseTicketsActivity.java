package com.example.mikael.cinemauclerc;

/**************************************************************************************************
 File name :BaseTicketsActivity.java
 Version : 0.2
 Purpose : Application Android pour vérifier la validité de tickets acheter en ligne.
 Usages : see README
 Date : 29/02/2016
 Author : mikael.lemasson@orange.fr
 Android : 4.0.2
 License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
 ***************************************************************************************************/

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

public class BaseTicketsActivity extends AppCompatActivity {

    static final String TAG = "BaseTicketsActivity";
    static TicketsDatabase BDD;
    static TextView ListBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_ticket);
        this.BDD = new TicketsDatabase(this);
        ListBase = (TextView) findViewById(R.id.ListBaseTickets);
        ListBase.setText(BDD.getTickets());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    }

/*    private String getTickets(){
        Log.i(TAG, "getTickets");
        BDD.open();
        Cursor c = BDD.getAllTickets();
        if (c.moveToFirst()) {
            do {
                Log.i(TAG,
                        c.getInt(c.getColumnIndex(COL_ID)) + "," +
                                c.getString(c.getColumnIndex(COL_NUMTT))
                );
            }
            while (c.moveToNext());
        }
        c.close(); // fermeture du curseur
        BDD.close();
    }
*/
}