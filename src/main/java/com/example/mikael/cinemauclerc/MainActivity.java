package com.example.mikael.cinemauclerc;
/**************************************************************************************************
 File name :MainActivity.java
 Version : 0.2
 Purpose : Application Android pour vérifier la validité de tickets acheter en ligne.
 Usages : see README
 Date : 29/02/2016
 Author : mikael.lemasson@orange.fr
 Android : 4.0.2
License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
***************************************************************************************************/

import android.net.wifi.WifiConfiguration;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Toast;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.TextView;
import android.preference.PreferenceManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.preference.PreferenceFragment;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;

public class MainActivity extends AppCompatActivity {

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    static String strUrl;
    static final String TAG = "MainActivity";
    static TextView StatusCine;
    static String numticket;
    static TicketsDatabase BDD;

    public static String toString(boolean b) {
        return b ? "true" : "false";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        strUrl = SP.getString("urlserver", "NA");
        if (strUrl.endsWith("/")){
            strUrl = strUrl.substring(0,strUrl.length()-1);
        }
        if (SP.getBoolean("Droptable", false) ) {
            Log.i(TAG, "DROP");
            this.BDD.DropTickets();
            SP.edit().putBoolean("Droptable", false).commit();
        }
        Log.i(TAG, strUrl);
        setContentView(R.layout.activity_main);
        StatusCine = (TextView) findViewById(R.id.StatusCine);
        this.BDD = new TicketsDatabase(this);
    }

    public void scanBar(View v) {
        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            showDialog(MainActivity.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    public void scanQR(View v) {
        try {
            Intent intent = new Intent(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);
        } catch (ActivityNotFoundException anfe) {
            showDialog(MainActivity.this, "No Scanner Found", "Download a scanner code activity?", "Yes", "No").show();
        }
    }

    private static AlertDialog showDialog(final AppCompatActivity act, CharSequence title, CharSequence message, CharSequence buttonYes, CharSequence buttonNo) {
        AlertDialog.Builder downloadDialog = new AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Uri uri = Uri.parse("market://search?q=pname:" + "com.google.zxing.client.android");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    act.startActivity(intent);
                } catch (ActivityNotFoundException anfe) {

                }
            }
        });
        downloadDialog.setNegativeButton(buttonNo, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        return downloadDialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
                numticket = contents;
                StatusCine.setText("n°:" + numticket);
                StatusCine.setBackgroundResource(R.drawable.bg);
                new AsyncHttpTask().execute(strUrl + '/' + numticket);
            }
        }
    }

    public void config(View v) {
        Intent i = new Intent(this, Configuration.class);
        startActivity(i);
    }

    public void baseTickets(View v) {
        Intent i = new Intent(this, BaseTicketsActivity.class);
        startActivity(i);
/*        Log.i(TAG, "start");
        BDD.getTickets();
        Log.i(TAG, "end");*/
    }

    @Override
    public void onResume() {
        super.onResume();
    }
/***************************************************************************************************

    Classe qui interroge le serveur http

 **************************************************************************************************/
    private class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

        static final String TAG = "AsyncHttpTask";
        String TTValide = "KO";
        String TTDate = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            Toast.makeText(getApplicationContext(), "Début du traitement asynchrone", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Integer doInBackground(String... params) {
            InputStream inputStream = null;
            HttpURLConnection urlConnection = null;
            Integer result = 0;
            Log.i(TAG, params[0]);
            try {
                /* forming th java.net.URL object */
                URL url = new URL(params[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                 /* optional request header */
                urlConnection.setRequestProperty("Content-Type", "application/json");

                /* optional request header */
                urlConnection.setRequestProperty("Accept", "application/json");

                /* for Get request */
                urlConnection.setRequestMethod("GET");
                int statusCode = urlConnection.getResponseCode();

             /* 200 represents HTTP OK */
                if (statusCode == 200) {
                    Log.i(TAG, "OK");
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    String response = convertInputStreamToString(inputStream);
                    parseResult(response);
                    result = 1; // Successful
                } else {
                    Log.i(TAG, "KO");
                    result = 0; //"Failed to fetch data!";
                }
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
            }
            //Si pas de réponse du serveur
            if (result == 0) {
                //On tente notre chance en local
                result = CheckBDD();
            }
            Log.i(TAG, result.toString());
            return result; //"Failed to fetch data!";
        }

        @Override
        protected void onPostExecute(Integer result) {

            /* Download complete. Lets update UI */
            if (result == 1) {
                Log.i(TAG, "Réponse du serveur"+TTValide+"1");
                StatusCine.append("\nRéponse du serveur: " + TTValide);
                if(TTValide.contains("OK") ) {
                    Log.i(TAG, "Réponse OK");
                    StatusCine.setBackgroundResource(R.drawable.bgok);
                } else {
                    Log.i(TAG, "Réponse KO");
                    StatusCine.setBackgroundResource(R.drawable.bgko);
                }
            } else if(result == 2) {
                Log.e(TAG, "KO Deja");
                    StatusCine.append("\nDB: Déjà validé : KO\nDate: " + TTDate);
                StatusCine.setBackgroundResource(R.drawable.bgko);

            } else if(result == 3) {
                Log.e(TAG, "OK db");
                StatusCine.append("\nDB: Valide : OK");
                StatusCine.setBackgroundResource(R.drawable.bgok);

            } else  {
                Log.e(TAG, "Pas de réponse du serveur");
                StatusCine.append("\nPas de réponse du serveur");
                StatusCine.setBackgroundResource(R.drawable.bgko);

            }
        }

        private String convertInputStreamToString(InputStream inputStream) throws IOException {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            String result = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }
            /* Close Stream */
            if (null != inputStream) {
                inputStream.close();
            }
            return result;
        }

        private int CheckBDD() {
            Integer result = 0;
            BDD.open();
            Ticket findticket = BDD.getTicket(numticket);
                Log.i(TAG, "r:"+findticket.toString());
                Log.i(TAG, "r:"+findticket.getNumtt());
                if (findticket.getNumtt().equals(numticket)) {
//                  deja scan
                    Log.i(TAG, "deja");
                    TTDate = findticket.getDate();
                    findticket.setStatus("KO");
                    Log.i(TAG, "old" + findticket.toString());
                    long res = BDD.updateTicket(findticket);
                    Log.i(TAG, "r:" + res);
                    result = 2;
                } else {
                    Log.i(TAG, "pas deja");

                    Ticket newticket = new Ticket(1, numticket, "", "OK");
                    Log.i(TAG, "new" + newticket.toString());
                    long res = BDD.insertTicket(newticket);
                    Log.i(TAG, "r:" + res);
                    result = 3;
                }
                BDD.close();
            return result;
        }

    private void parseResult(String result) {
            try {
                JSONObject response = new JSONObject(result);
                Log.i(TAG,"s:"+response.getString("status"));
                Log.i(TAG,"noj:"+response.getString("no"));
                Log.i(TAG, "no" + numticket);
                Log.i(TAG, "v:" + response.getString("valide"));
                Log.i(TAG, "m:" + response.getString("msg"));

                int cmp =response.getString("no").compareTo(numticket);
                if( cmp == 0 ) {
                    TTValide = response.getString("valide")+"\n"+response.getString("msg");
                    BDD.open();
                    Ticket newticket = new Ticket(1, numticket, "", response.getString("valide"));
                    Log.i(TAG, "new" + newticket.toString());
                    long res = BDD.insertTicket(newticket);
                    BDD.close();
                } else {
                    TTValide = "KO";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}