package com.example.mikael.cinemauclerc;

/**************************************************************************************************
 File name :TicketsDatabase.java
 Version : 0.2
 Purpose : Application Android pour vérifier la validité de tickets acheter en ligne.
 Usages : see README
 Date : 29/02/2016
 Author : mikael.lemasson@orange.fr
 Android : 4.0.2
 License : WTFPL – Do What the Fuck You Want to Public License http:www.wtfpl.net
 ***************************************************************************************************/

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class TicketsDatabase {

    static final String TAG = "TicketsDatabase";
    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "tickets.db";

    private static final String TABLE_TICKETS = "table_tickets";
    private static final String COL_ID = "ID";
    private static final int NUM_COL_ID = 0;
    private static final String COL_NUMTT = "NUMTT";
    private static final int NUM_COL_NUMTT = 1;
    private static final String COL_DATE = "DATE";
    private static final int NUM_COL_DATE = 2;
    private static final String COL_STATUS = "STATUS";
    private static final int NUM_COL_STATUS = 3;

    private SQLiteDatabase bdd;

    private DatabaseHandler maBaseSQLite;

    public TicketsDatabase(Context context){
        //On créer la BDD et sa table
        maBaseSQLite = new DatabaseHandler(context, NOM_BDD, null, VERSION_BDD);
    }

    public void open(){
        //on ouvre la BDD en écriture
        bdd = maBaseSQLite.getWritableDatabase();
    }

    public void close(){
        //on ferme l'accès à la BDD
        bdd.close();
    }

    public SQLiteDatabase getBDD(){
        return bdd;
    }

    public long insertTicket(Ticket ticket){
        //Création d'un ContentValues (fonctionne comme une HashMap)
        ContentValues values = new ContentValues();
        //on lui ajoute une valeur associé à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
//        values.put(COL_ID, ticket.getId());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        GregorianCalendar todayDate = new GregorianCalendar();
        Log.i(TAG, "formatted todayDate: " + sdf.format(todayDate.getTime()));
        values.put(COL_NUMTT, ticket.getNumtt());
        values.put(COL_DATE, sdf.format(todayDate.getTime()));
        values.put(COL_STATUS, ticket.getStatus());
        //on insère l'objet dans la BDD via le ContentValues
        return bdd.insert(TABLE_TICKETS, null, values);
    }


    public int updateTicket(Ticket ticket){
        //La mise à jour d'un Ticket dans la BDD fonctionne plus ou moins comme une insertion
        //il faut simple préciser quelle ticket on doit mettre à jour grâce à l'ID
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        GregorianCalendar todayDate = new GregorianCalendar();
        Log.i(TAG, "formatted todayDate: " + sdf.format(todayDate.getTime()));
        ContentValues values = new ContentValues();
        values.put(COL_STATUS, ticket.getStatus());
        values.put(COL_DATE, sdf.format(todayDate.getTime()));
        Log.i(TAG, "v:" + values.toString());
        Log.i(TAG,"tt:"+ticket.getId());
        return bdd.update(TABLE_TICKETS, values, COL_ID + " = " +ticket.getId(), null);
    }


    public Ticket getTicket(String numtt){
        //Récupère dans un Cursor les valeur correspondant à un ticket contenu dans la BDD
        Cursor c = bdd.query(TABLE_TICKETS, new String[] {COL_ID, COL_NUMTT, COL_DATE, COL_STATUS}, COL_NUMTT + " = \"" + numtt +"\"", null, null, null, null);
        return cursorToTicket(c);
    }

    //Cette méthode permet de convertir un cursor en un ticket
    private Ticket cursorToTicket(Cursor c){
        //si aucun élément n'a été retourné dans la requête, on renvoie null
        if (c.getCount() == 0)
            return new Ticket();

        //Sinon on se place sur le premier élément
        c.moveToFirst();
        //On créé un ticket
        Ticket ticket = new Ticket();
        //on lui affecte toutes les infos grâce aux infos contenues dans le Cursor
        ticket.setId(c.getInt(NUM_COL_ID));
        ticket.setNumtt(c.getString(NUM_COL_NUMTT));
        ticket.setDate(c.getString(NUM_COL_DATE));
        ticket.setStatus(c.getString(NUM_COL_STATUS));
        //On ferme le cursor
        c.close();

        //On retourne le ticket
        return ticket;
    }

    public void DropTickets(){
        this.open();
        String sql = "DROP TABLE IF EXISTS " + TABLE_TICKETS + ";";
        try {
            bdd.execSQL(sql);
            Log.i(TAG, "s:" + sql);
        } catch (SQLException e) {
            Log.e(TAG, "s:" + sql);
        }
        maBaseSQLite.onCreate(bdd);
        this.close();
    }

    public String getTickets() {
        String result ="";
        Log.i(TAG, "getTickets");
        this.open();
        Cursor c = this.getAllTickets();
        if (c.moveToFirst()) {
            do {
                Log.i(TAG,
                        c.getInt(c.getColumnIndex(COL_ID)) + "," +
                                c.getString(c.getColumnIndex(COL_NUMTT)) + ","+
                                c.getString(c.getColumnIndex(COL_DATE)) + ","+
                                c.getString(c.getColumnIndex(COL_STATUS))
                );
                result = result + c.getString(c.getColumnIndex(COL_NUMTT)) + ","+
                        c.getString(c.getColumnIndex(COL_DATE))+ ","+
                        c.getString(c.getColumnIndex(COL_STATUS))+ "\n";
            }
            while (c.moveToNext());
        }
        c.close(); // fermeture du curseur
        this.close();
        return result;
    }

    private Cursor getAllTickets() {
        // sélection de tous les enregistrements de la table
        return bdd.rawQuery("SELECT * FROM "+TABLE_TICKETS+" ORDER BY "+COL_ID+" DESC;", null);
    }
}